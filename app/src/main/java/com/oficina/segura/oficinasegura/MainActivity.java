package com.oficina.segura.oficinasegura;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.installations.FirebaseInstallations;
import com.google.firebase.installations.InstallationTokenResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;

import static android.content.ContentValues.TAG;
import static com.oficina.segura.oficinasegura.R.menu.navigation;

import android.app.Activity;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    WebView myWebView;
    String tipo = "";
    String usuario = "";
    String codigo = "";
    String deviceId = "";
    String token = "";
    Context cont;
    private ValueCallback<Uri> mUploadMessage;
    private final static int FILECHOOSER_RESULTCODE=1;
    public ValueCallback<Uri[]> uploadMessage;
    public static final int REQUEST_SELECT_FILE = 100;


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (myWebView.canGoBack()) {
                        myWebView.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            if (requestCode == REQUEST_SELECT_FILE)
            {
                if (uploadMessage == null)
                    return;
                uploadMessage.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, data));
                uploadMessage = null;
            }
        }
        else if (requestCode == FILECHOOSER_RESULTCODE)
        {
            if (null == mUploadMessage)
                return;
            // Use MainActivity.RESULT_OK if you're implementing WebView inside Fragment
            // Use RESULT_OK only if you're implementing WebView inside an Activity
            Uri result = data == null || resultCode != MainActivity.RESULT_OK ? null : data.getData();
            mUploadMessage.onReceiveValue(result);
            mUploadMessage = null;
        }
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Log.e("Scan*******", "Cancelled scan");

            } else {
                Log.e("Scan", "Scanned");


                if (tipo == "checkin") {
                    String text = (result.getContents());

                    myWebView.loadUrl("https://demo.oficinasegura.com/mobile/checkinpuesto?codigo=" + text);
                    //new CallAPI(cont,usuario,text).execute(text);
                } else if (tipo == "validar") {
                    String text = (result.getContents());

                    myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_validacion?id=" + text);

                }else if (tipo == "asistencia") {
                    String text = (result.getContents());

                    myWebView.loadUrl("https://demo.oficinasegura.com/mobile/asistenciaedificio?id=" + text + "&id_usuario=" + usuario);

                }else if (tipo == "asistenciaout") {
                    String text = (result.getContents());

                    myWebView.loadUrl("https://demo.oficinasegura.com/mobile/asistenciaoutedificio?id=" + text + "&id_usuario=" + usuario);

                } else if (tipo == "limpiar") {
                    String text = (result.getContents());

                    myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_limpiar?id=" + text);

                } else if (tipo == "incidencia") {
                    String text = (result.getContents());

                    myWebView.loadUrl("https://demo.oficinasegura.com/mobile/incidencia?idbase=" + text);

                } else if (tipo == "hotcheck") {
                    String text = (result.getContents());

                    myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_hotresultado?id=" + text);

                } else if (tipo == "checkout") {
                    String text = (result.getContents());
                    myWebView.loadUrl("https://demo.oficinasegura.com/mobile/checkoutpuesto?codigo=" + text);
                    // new CallOut(cont,usuario,text).execute(text,usuario);

                }
            }
        } else {
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState )
    {
        super.onSaveInstanceState(outState);
        myWebView.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        myWebView.restoreState(savedInstanceState);
    }

    public static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        myWebView = (WebView) findViewById(R.id.webview);



        cont = this;

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        if (!isNetworkAvailable()){
            Intent intent = new Intent(MainActivity.this, OffActivity.class);
            startActivity(intent);
            finish();
        }

        TelephonyManager telephonyManager;
        telephonyManager = (TelephonyManager) getSystemService(Context.
                TELEPHONY_SERVICE);

        deviceId = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);

        if (deviceId==null || deviceId == ""){
            Environment.getExternalStorageDirectory();


            String yourFile = getExternalFilesDir(null)+"id.txt";
            deviceId =  String.valueOf(new Random().nextInt());

            File tmpDir = new File(yourFile);
            boolean exists = tmpDir.exists();



            if(!exists) {
                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(yourFile);
                    fos.write(deviceId.getBytes());
                    fos.flush();
                    fos.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else{
                File fl = new File(yourFile);
                FileInputStream fin = null;
                try {
                    fin = new FileInputStream(fl);
                    String ret = convertStreamToString(fin);
                    //Make sure you close all streams.
                    fin.close();

                    deviceId=ret;
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

        deviceId = deviceId.replaceAll(System.getProperty("line.separator"), "");
        deviceId = deviceId.replaceAll("\n","");
        deviceId = deviceId.replaceAll("-","");

        FirebaseApp.initializeApp(this);

        FirebaseInstallations.getInstance().getToken(false).addOnCompleteListener(new OnCompleteListener<InstallationTokenResult>() {
            @Override
            public void onComplete(@NonNull Task<InstallationTokenResult> task) {
                if(!task.isSuccessful()){
                    return;
                }
                // Get new Instance ID token
                token = task.getResult().getToken();
                myWebView.loadUrl("https://demo.oficinasegura.com/mobile/inicio?id="+deviceId+"&token="+token);
                new RegistrarToken(cont,deviceId,token).execute();

            }
        });

        FirebaseMessaging.getInstance().subscribeToTopic(deviceId)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        String msg = "Te has subscripto para recibir notificaciones";
                        if (!task.isSuccessful()) {
                            msg = "Tu subscripcion a las notificaciones falló";
                        }
                        Log.d(TAG, msg);
                        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });



        myWebView.setWebViewClient(new WebViewClient(){



            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (!isNetworkAvailable()){
                    Intent intent = new Intent(MainActivity.this, OffActivity.class);
                    startActivity(intent);
                    finish();
                }
                if (url != null && (url.contains("mobile/inicio"))) {

                    myWebView.loadUrl("https://demo.oficinasegura.com/mobile/inicio?id="+deviceId+"&token="+token);
                   return false;
                  }
                  else if (url.contains("mobile/login")){
                    return false;

                }else if (url.contains("whatsapp")) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    view.getContext().startActivity(intent);
                    return true;
                }
                else if (url.contains("maps")) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    view.getContext().startActivity(intent);
                    return true;
                }
                  else if (url.contains("checkinconqr")){
                    IntentIntegrator integrator = new IntentIntegrator(MainActivity.this);
                    integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                    integrator.setPrompt("Scan");
                    tipo = "checkin";
                    integrator.setCameraId(0);
                    integrator.setBeepEnabled(false);
                    integrator.setBarcodeImageEnabled(false);
                    integrator.initiateScan();

                    myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_procesando");
                    return false;

                  }
                else if (url.contains("asistenciaqr")){
                    IntentIntegrator integrator = new IntentIntegrator(MainActivity.this);
                    integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                    integrator.setPrompt("Scan");
                    tipo = "asistencia";
                    integrator.setCameraId(0);
                    integrator.setBeepEnabled(false);
                    integrator.setBarcodeImageEnabled(false);
                    integrator.initiateScan();

                    myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_procesando");
                    return false;

                }
                else if (url.contains("asistenciaoutqr")){
                    IntentIntegrator integrator = new IntentIntegrator(MainActivity.this);
                    integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                    integrator.setPrompt("Scan");
                    tipo = "asistenciaout";
                    integrator.setCameraId(0);
                    integrator.setBeepEnabled(false);
                    integrator.setBarcodeImageEnabled(false);
                    integrator.initiateScan();

                    myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_procesando");
                    return false;

                }
                else if (url.contains("incidenciaconqr")){
                    IntentIntegrator integrator = new IntentIntegrator(MainActivity.this);
                    integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                    integrator.setPrompt("Scan");
                    tipo = "incidencia";
                    integrator.setCameraId(0);
                    integrator.setBeepEnabled(false);
                    integrator.setBarcodeImageEnabled(false);
                    integrator.initiateScan();

                    myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_procesando");
                    return false;

                }
                else if (url.contains("checkoutconqr")){
                    IntentIntegrator integrator = new IntentIntegrator(MainActivity.this);
                    integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                    integrator.setPrompt("Scan");
                    tipo = "checkout";
                    integrator.setCameraId(0);
                    integrator.setBeepEnabled(false);
                    integrator.setBarcodeImageEnabled(false);
                    integrator.initiateScan();

                    myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_procesando");
                    return false;

                }
                  else if (url.contains("validarconqr")){
                    IntentIntegrator integrator = new IntentIntegrator(MainActivity.this);
                    integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                    integrator.setPrompt("Scan");
                    tipo = "validar";
                    integrator.setCameraId(0);
                    integrator.setBeepEnabled(false);
                    integrator.setBarcodeImageEnabled(false);
                    integrator.initiateScan();

                    myWebView.loadUrl("https://demo.oficinasegura.com/mobile/validar");
                    return false;

                }
                else if (url.contains("limpiarconqr")){
                    IntentIntegrator integrator = new IntentIntegrator(MainActivity.this);
                    integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                    integrator.setPrompt("Scan");
                    tipo = "limpiar";
                    integrator.setCameraId(0);
                    integrator.setBeepEnabled(false);
                    integrator.setBarcodeImageEnabled(false);
                    integrator.initiateScan();

                    myWebView.loadUrl("https://demo.oficinasegura.com/mobile/mobile_limpieza");
                    return false;

                }
                else if (url.contains("checkinconnfc")){
                    Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                    Bundle b = new Bundle();
                    b.putString("usuario", usuario); //Your id
                    intent.putExtras(b); //Put your id to your next Intent
                    startActivity(intent);
                    finish();
                    myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_procesando");
                    return false;
                }
                else if (url.contains("asistencianfc")){
                    Intent intent = new Intent(MainActivity.this, AsistenciaActivity.class);
                    Bundle b = new Bundle();
                    b.putString("usuario", usuario); //Your id
                    intent.putExtras(b); //Put your id to your next Intent
                    startActivity(intent);
                    finish();
                    myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_procesando");
                    return false;
                }
                else if (url.contains("asistenciaoutnfc")){
                    Intent intent = new Intent(MainActivity.this, AsistenciaOutActivity.class);
                    Bundle b = new Bundle();
                    b.putString("usuario", usuario); //Your id
                    intent.putExtras(b); //Put your id to your next Intent
                    startActivity(intent);
                    finish();
                    myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_procesando");
                    return false;
                }
                else if (url.contains("incidenciaconnfc")){
                    Intent intent = new Intent(MainActivity.this, IncidenciaActivity.class);
                    Bundle b = new Bundle();
                    b.putString("usuario", usuario); //Your id
                    intent.putExtras(b); //Put your id to your next Intent
                    startActivity(intent);
                    finish();
                    myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_procesando");
                    return false;
                }
                  else if (url.contains("hotconnfc")){
                    Intent intent = new Intent(MainActivity.this, HotActivity.class);
                    Bundle b = new Bundle();
                    b.putString("usuario", usuario); //Your id
                    intent.putExtras(b); //Put your id to your next Intent
                    startActivity(intent);
                    finish();
                    myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_procesando");
                    return false;
                }
                else if (url.contains("hotconqr")){
                    IntentIntegrator integrator = new IntentIntegrator(MainActivity.this);
                    integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                    integrator.setPrompt("Scan");
                    tipo = "hotcheck";
                    integrator.setCameraId(0);
                    integrator.setBeepEnabled(false);
                    integrator.setBarcodeImageEnabled(false);
                    integrator.initiateScan();
                    myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_procesando");
                    return false;
                }
                else if (url.contains("checkoutconnfc")){
                    Intent intent = new Intent(MainActivity.this, OutActivity.class);
                    Bundle b = new Bundle();
                    b.putString("usuario", usuario); //Your id
                    intent.putExtras(b); //Put your id to your next Intent
                    startActivity(intent);
                    finish();
                    myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_procesando");
                    return false;
                }
                else if (url.contains("limpiarconnfc")){
                    Intent intent = new Intent(MainActivity.this, LimpiarActivity.class);
                    Bundle b = new Bundle();
                    b.putString("usuario", usuario); //Your id
                    intent.putExtras(b); //Put your id to your next Intent
                    startActivity(intent);
                    finish();
                    myWebView.loadUrl("https://demo.oficinasegura.com/mobile/mobile_limpieza");
                    return false;
                }
                else {
                    if (usuario==""){
                        new CallUsuario(cont).execute(deviceId);
                    }
                    return false;
                }
            }
        });

        myWebView.setWebChromeClient(new WebChromeClient()
        {
            // For 3.0+ Devices (Start)
            // onActivityResult attached before constructor
            protected void openFileChooser(ValueCallback uploadMsg, String acceptType)
            {
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                startActivityForResult(Intent.createChooser(i, "File Browser"), FILECHOOSER_RESULTCODE);
            }


            // For Lollipop 5.0+ Devices
            public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams)
            {
                if (uploadMessage != null) {
                    uploadMessage.onReceiveValue(null);
                    uploadMessage = null;
                }

                uploadMessage = filePathCallback;

                Intent intent = fileChooserParams.createIntent();
                try
                {
                    startActivityForResult(intent, REQUEST_SELECT_FILE);
                } catch (ActivityNotFoundException e)
                {
                    uploadMessage = null;
                    return false;
                }
                return true;
            }

            //For Android 4.1 only
            protected void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture)
            {
                mUploadMessage = uploadMsg;
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "File Browser"), FILECHOOSER_RESULTCODE);
            }

            protected void openFileChooser(ValueCallback<Uri> uploadMsg)
            {
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                startActivityForResult(Intent.createChooser(i, "File Chooser"), FILECHOOSER_RESULTCODE);
            }

        });

        WebSettings webSettings = myWebView.getSettings();
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        Bundle b = getIntent().getExtras();
        int value = -1; // or other values
        if(b != null) {
            value = b.getInt("key");
            String usu = b.getString("usuario");

            if (usu!=null && usu !="")
                usuario = usu;

            String cod = b.getString("codigo");

            if (usu!=null && usu !="")
                codigo = cod;
        }

        if (value==1) {
            new CallAPI(cont,usuario,codigo).execute();
            myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_procesando");
        }
        else if (value==2) {
            new CallOut(cont,usuario,codigo).execute();
            myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_procesando");
        }
        else if (value==3) {
            procesarLimpiar(codigo);
        }
        else if (value==4) {
            myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_hotresultado?id="+codigo);
        }
        else if (value==5) {
            myWebView.loadUrl("https://demo.oficinasegura.com/mobile/incidencia?idbase="+codigo);
        }
        else if (value==6) {
            myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_incidenciaresultado?id="+codigo + "&id_usuario=" + usuario);
        }
        else if (value==7) {
            myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_incidenciaoutresultado?id="+codigo + "&id_usuario=" + usuario);
        }
        else if (value==0)
            myWebView.loadUrl("https://demo.oficinasegura.com/mobile/reservas");
        else
            if (savedInstanceState == null) {

                myWebView.loadUrl("https://demo.oficinasegura.com/mobile/inicio?id="+deviceId+"&token="+token);
            }


    }

    void procesarUsuario(String us) {
        usuario = us;
    }

    void procesarCheckout(String rta) {
        if (rta.equals("1"))
            myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_checkout");
        else if (rta.equals("2"))
            myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_checkout_error?id=2");
        else
            myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_checkout_error?id=3");
    }

    void procesarLimpiar(String rta) {
       myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_limpiar?id="+rta);
    }

    void procesarCheckin(String rta) {
        if (rta.equals("1"))
            myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_curso");
        else if (rta.equals("2"))
            myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_checkin_error?id=2");
        else if (rta.equals("3"))
            myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_checkin_error?id=3");
        else
            myWebView.loadUrl("https://demo.oficinasegura.com/mobile/m_checkin_error?id=4");
    }




    class CallUsuario extends AsyncTask<String, String, String> {

        Context contin;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        public CallUsuario(Context cont){
            contin = cont;
            //set context variables if required
        }

        @Override
        protected void onPostExecute(String result) {
            procesarUsuario(result);


        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URL url = new URL("https://demo.oficinasegura.com/mobile/usuario");
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                try {
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("POST");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);

                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("ids", params[0]);

                    String query = builder.build().getEncodedQuery();

                    conn.setFixedLengthStreamingMode(query.getBytes().length);

                    OutputStream os = conn.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();

                    InputStream inputStream = new BufferedInputStream(conn.getInputStream());
                    usuario = convertStreamToString(inputStream);
                    usuario = usuario.replace("\n","");


                    return usuario;
                }
                finally {
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        public String convertStreamToString(InputStream is) {

            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();

            String line = null;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append((line + "\n"));
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return sb.toString();
        }
    }

    class CallAPI extends AsyncTask<String, String, String> {

        Context contin;
        String respuesta, usuario, codigo;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        public CallAPI(Context cont, String usu, String cod){
            contin = cont;
            usuario = usu;
            codigo = cod;
            //set context variables if required
        }

        @Override
        protected void onPostExecute(String result) {
            procesarCheckin(result);


        }


        @Override
        protected String doInBackground(String... params) {
            try {
                URL url = new URL("https://demo.oficinasegura.com/mobile/checkinpuesto");
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                try {
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("POST");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);


                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("codigo", codigo.replace("=","_")).appendQueryParameter("id_usuario", usuario);

                    String query = builder.build().getQuery();

                    conn.setFixedLengthStreamingMode(query.getBytes().length);

                    OutputStream os = conn.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();

                    InputStream inputStream = new BufferedInputStream(conn.getInputStream());
                    respuesta = convertStreamToString(inputStream);

                    respuesta = respuesta.replace("\n","");
                    return respuesta;
                }
                finally {
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        public String convertStreamToString(InputStream is) {

            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();

            String line = null;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append((line + "\n"));
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return sb.toString();
        }
    }


    class CallValidar extends AsyncTask<String, String, String> {

        AlertDialog alertDialog;
        Context contin;
        String respuesta;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialog = new AlertDialog.Builder(contin).create();
        }

        public CallValidar(Context cont){
            contin = cont;
            //set context variables if required
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            alertDialog.setTitle("Validación de Reserva");
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(respuesta);
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
            alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {

                }
            });
            alertDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URL url = new URL("https://demo.oficinasegura.com/mobile/validarreserva");
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                try {
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("POST");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);

                    ContentValues values = new ContentValues();
                    values.put("codigo",params[0]);
                    values.put("id_usuario",params[1]);

                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("codigo", params[0]).appendQueryParameter("id_usuario", params[1]);

                    String query = builder.build().getEncodedQuery();

                    conn.setFixedLengthStreamingMode(query.getBytes().length);

                    OutputStream os = conn.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();

                    InputStream inputStream = new BufferedInputStream(conn.getInputStream());
                    respuesta = convertStreamToString(inputStream);

                    conn.connect();

                    conn.connect();
                }
                finally {
                    conn.disconnect();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        public String convertStreamToString(InputStream is) {

            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();

            String line = null;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append((line + "\n"));
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return sb.toString();
        }
    }


    class RegistrarToken extends AsyncTask<String, String, String> {

        AlertDialog alertDialog;
        Context contin;
        String respuesta, device_id,token;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialog = new AlertDialog.Builder(contin).create();
        }

        public RegistrarToken(Context cont,String did, String tok){
            contin = cont;
            device_id = did;
            token = tok;
            //set context variables if required
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URL url = new URL("https://demo.oficinasegura.com/mobile/registrar_token");
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                try {
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("POST");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);

                    ContentValues values = new ContentValues();
                    values.put("device_id",device_id);
                    values.put("token",token);

                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("device_id", device_id).appendQueryParameter("token", token);

                    String query = builder.build().getEncodedQuery();

                    conn.setFixedLengthStreamingMode(query.getBytes().length);

                    OutputStream os = conn.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();

                    InputStream inputStream = new BufferedInputStream(conn.getInputStream());
                    respuesta = convertStreamToString(inputStream);

                    conn.connect();

                }
                finally {
                    conn.disconnect();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        public String convertStreamToString(InputStream is) {

            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();

            String line = null;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append((line + "\n"));
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return sb.toString();
        }
    }


    class CallOut extends AsyncTask<String, String, String> {

        Context contin;
        String respuesta, usuario, codigo;
        @Override
        protected void onPreExecute() {
        }

        public CallOut(Context cont, String usu, String cod){
            contin = cont;
            usuario = usu;
            codigo = cod;
            //set context variables if required
        }

        @Override
        protected void onPostExecute(String result) {
            procesarCheckout(result);

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URL url = new URL("https://demo.oficinasegura.com/mobile/checkoutpuesto");
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                try {
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);


                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("codigo", codigo.replace("=","_")).appendQueryParameter("id_usuario", usuario);

                    String query = builder.build().getEncodedQuery();

                    conn.setFixedLengthStreamingMode(query.getBytes().length);

                    OutputStream os = conn.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();

                    InputStream inputStream = new BufferedInputStream(conn.getInputStream());
                    respuesta = convertStreamToString(inputStream);
                    respuesta = respuesta.replace("\n","");
                    return respuesta;
                }
                finally {
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        public String convertStreamToString(InputStream is) {

            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();

            String line = null;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append((line + "\n"));
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return sb.toString();
        }
    }

}








